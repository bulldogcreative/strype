<?php

namespace Bulldog\Strype\Contracts;

interface ResourceInterface
{
    public function getId(): string;
}
