# Strype

[![Build Status](https://travis-ci.org/bulldogcreative/strype.svg?branch=master)](https://travis-ci.org/bulldogcreative/strype)
[![Coverage Status](https://coveralls.io/repos/github/bulldogcreative/strype/badge.svg?branch=master)](https://coveralls.io/github/bulldogcreative/strype?branch=master)
[![CodeFactor](https://www.codefactor.io/repository/github/bulldogcreative/strype/badge)](https://www.codefactor.io/repository/github/bulldogcreative/strype)

A wrapper for the Stripe SDK that utilizes non-static methods.

[Strype API docs](https://docs.bulldog.cloud/strype/v0.9.0/).

## Development

This is still under active development and the API may change without notice.
Please feel free to help write code, tests, or report bugs. The primary goal
of this project is to create an easily testable wrapper for the Stripe SDK.
